#include <stdio.h>
#include <string.h>
#include "library.h"


void user_accept(user_t *u) 
{
	//printf("id: ");
	//scanf("%d", &u->id);
	u->id = get_next_user_id();
	printf("name: ");
	scanf("%s", u->name);
	printf("email: ");
	scanf("%s", u->email);
	printf("phone: ");
	scanf("%s", u->phone);
	printf("password: ");
	scanf("%s", u->password);
	strcpy(u->role, ROLE_MEMBER);
}
void user_display(user_t *u) 
{
	printf("%d, %s, %s, %s, %s\n", u->id, u->name, u->email, u->phone, u->role);
}

void book_accept(book_t *b) 
{
	//printf("id: ");
	//scanf("%d", &b->id);
	printf("name: ");
	scanf("%s", b->name);
	printf("author: ");
	scanf("%s", b->author);
	printf("subject: ");
	scanf("%s", b->subject);
	printf("price: ");
	scanf("%f", &b->price);
	printf("isbn: ");
	scanf("%s", b->isbn);
}

void book_display(book_t *b) 
{
	printf("BookID:%d, %s, %s, %s, %.2f, %s\n", b->id, b->name, b->author, b->subject, b->price, b->isbn);
}

void bookcopy_accept(bookcopy_t *c)
{
	// printf("id: ");
	// scanf("%d", &c->id);
	printf("Book id: ");
	scanf("%d",&c->bookid);
	printf("Rack : ");
	scanf("%d",&c->rack);
	strcpy(c->status,STATUS_AVAIL);

}
void bookcopy_display(bookcopy_t *c)
{
	printf("copyId:%d,BookId:%d,Rack:%d,Status:%s\n",c->id,c->bookid,c->rack,c->status);
}

void issuerecord_accept(issuerecord_t *r) 
{
	// printf("id: ");
	// scanf("%d", &r->id);
	printf("copy id: ");
	scanf("%d", &r->copyid);
	printf("member id: ");
	scanf("%d", &r->memberid);
	printf("issue ");
	date_accept(&r->issue_date);
	//r->issue_date = date_current();
	r->return_duedate = date_add(r->issue_date, BOOK_RETURN_DAYS);
	memset(&r->return_date, 0, sizeof(date_t));
	r->fine_amount = 0.0;
}

void issuerecord_display(issuerecord_t *r) 
{
	printf("issue record: %d, copy: %d, member: %d, find: %.2lf\n", r->id, r->copyid, r->memberid, r->fine_amount);
	printf("issue ");
	date_print(&r->issue_date);
	printf("return due ");
	date_print(&r->return_duedate);
	printf("return ");
	date_print(&r->return_date);
}

void payment_accept(payment_t *p) 
{
	//printf("id: ");
	//scanf("%d", &p->id);
	printf("member id: ");
	scanf("%d", &p->memberid);
	//printf("type (fees/fine): ");
	//scanf("%s", p->type);
	strcpy(p->type,PAY_TYPE_FEES);
	printf("amount: ");
	scanf("%lf", &p->amount);
	p->tx_time = date_current();
	//if(strcmp(p->type, PAY_TYPE_FEES) == 0)
		p->next_pay_duedate = date_add(p->tx_time, MEMBERSHIP_MONTH_DAYS);
	//else
		//memset(&p->next_pay_duedate, 0, sizeof(date_t));
}

void payment_display(payment_t *p) 
{
	printf("payment: %d, member: %d, %s, amount: %.2lf\n", p->id, p->memberid, p->type, p->amount);
	printf("payment ");
	date_print(&p->tx_time);
	printf("payment due");
	date_print(&p->next_pay_duedate);
}
void user_add(user_t *u) 
{
	FILE *fp;
	fp = fopen(USER_DB, "ab");
	if(fp == NULL) 
    {
		perror("failed to open users file");
		return;
	}
	fwrite(u, sizeof(user_t), 1, fp);
	printf("user added into file.\n");
	fclose(fp);
}

void book_find_by_name(char name[]) 
{
	FILE *fp;
	int found = 0;
	book_t b;
	fp = fopen(BOOK_DB, "rb");
	if(fp == NULL) 
	{
		perror("failed to open books file");
		return;
	}
	while(fread(&b, sizeof(book_t), 1, fp) > 0) 
	{
		if(strstr(b.name, name) != NULL) 
		{
			found = 1;
			book_display(&b);
		}
	}
	fclose(fp);
	if(!found)
		printf("No such book found.\n");
}

int user_find_by_email(user_t *u, char email[]) 
{
	FILE *fp;
	int found = 0;
	fp = fopen(USER_DB, "rb");
	if(fp == NULL) 
    {
		perror("failed to open users file");
		return found;
	}
	while(fread(u, sizeof(user_t), 1, fp) > 0) 
    {
		// if user email is matching, found 1
		if(strcmp(u->email, email) == 0) 
		{
			found = 1;
			break;
		}
	}
	fclose(fp);
	// return
	return found;
}

int get_next_user_id() 
{
	FILE *fp;
	int max = 0;
	int size = sizeof(user_t);
	user_t u;
	fp = fopen(USER_DB, "rb");
	if(fp == NULL)
		return max + 1;
	fseek(fp, -size, SEEK_END);
	if(fread(&u, size, 1, fp) > 0)
		max = u.id;
	fclose(fp);
	return max + 1;
}

int get_next_book_id() 
{
	FILE *fp;
	int max = 0;
	int size = sizeof(book_t);
	book_t u;
	fp = fopen(BOOK_DB, "rb");
	if(fp == NULL)
		return max + 1;
	fseek(fp, -size, SEEK_END);
	if(fread(&u, size, 1, fp) > 0)
		max = u.id;
	fclose(fp);
	return max + 1;
}

int get_next_bookcopy_id()
{
	int max=0;
	bookcopy_t bc;
	long size=sizeof(bookcopy_t);
	FILE *fp;
	fp=fopen(BOOKCOPY_DB,"rb");
	if(fp==NULL)
	{
		return max+1;
	}
	fseek(fp,-size,SEEK_END);
	fread(&bc,size,1,fp);
	max=bc.id;
	fclose(fp);
	return max+1;
}

int get_next_issuerecord_id() 
{
	FILE *fp;
	int max = 0;
	int size = sizeof(issuerecord_t);
	issuerecord_t u;
	fp = fopen(ISSUERECORD_DB, "rb");
	if(fp == NULL)
		return max + 1;
	fseek(fp, -size, SEEK_END);
	if(fread(&u, size, 1, fp) > 0)
		max = u.id;
	fclose(fp);
	return max + 1;
}

int get_next_payment_id() 
{
	FILE *fp;
	int max = 0;
	int size = sizeof(payment_t);
	payment_t u;
	fp = fopen(PAYMENT_DB, "rb");
	if(fp == NULL)
		return max + 1;
	fseek(fp, -size, SEEK_END);
	if(fread(&u, size, 1, fp) > 0)
		max = u.id;
	fclose(fp);
	return max + 1;
}

void edit_profile(user_t *u)
{
    user_t nu;
    int found=0;
    long size= sizeof(user_t);
	FILE *fp= fopen(USER_DB,"rb+");
    if(fp==NULL)
    {
        perror("File can't be opened...\n");
        return;
    }
    while(fread(&nu,size,1,fp)>0)
    {
        if(nu.id==u->id)
        {
            found=1;
            break;
        }
    }
    if(found)
    {
        char name[30],phone[15],email[20];
        int choice;
        do{
             printf("\n0. out\n1. change name\n2. change phone\n3. change email\nenter choice ");
            scanf("%d",&choice);
            switch(choice)
            {
                case 1:
                    printf("Enter new name: ");
                    scanf("%s",name);
                    strcpy(nu.name,name);
                    break;
                case 2:
                    printf("Enter new phone: ");
                    scanf("%s",phone);
                    strcpy(nu.phone,phone);
                    break;
                case 3:
                    printf("Enter new email: ");
                    scanf("%s",email);
                    strcpy(nu.email,email);
                    printf("Email is updated..\n");
                    break;

            }
        }while(choice!=0);
        fseek(fp,-size,SEEK_CUR);
        user_display(&nu);
        fwrite(&nu,size,1,fp);
       
    }
    else
    {
        printf("No such uder is found..\n");
    }
	fclose(fp);
    
}

void edit_password(user_t *u){
    user_t nu;
    int found=0;
    long size= sizeof(user_t);
    
    FILE *fp= fopen(USER_DB,"rb+");
    if(fp==NULL)
    {
        perror("File can't be opened...\n");
        return;
    }
    while(fread(&nu,size,1,fp)>0)
    {
        if(nu.id==u->id)
        {
            found=1;
            break;
        }
    }
    if(found)
    {
        char password[15];
        printf("Enter new password: ");
        scanf("%s",password);
       
        fseek(fp,-size,SEEK_CUR);
        strcpy(nu.password,password);
        fwrite(&nu,size,1,fp);
        user_display(&nu);
        
        printf("password is updated..\n");
        
       
    }
    else
    {
        printf("No such uder is found..\n");
    }
	fclose(fp);

}



